function guardarEnLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  localStorage.setItem(clave, valor);
  var objeto = {
    nombre:"Gilberto",
    apellidos:"Meza Hoces",
    ciudad:"Lima",
    pais:"Perú"
  };
  localStorage.setItem("json", JSON.stringify(objeto));
}
function leerDeLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = localStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
  var datosUsuario = JSON.parse(localStorage.getItem("json"));
  if(datosUsuario!=null){
	  console.log(datosUsuario.nombre);
	  console.log(datosUsuario.pais);
	  console.log(datosUsuario);
  }
}
function removerDeLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  localStorage.removeItem(clave);
  alert('Removido');
}
function limpiarDeLocalStorage() {
  localStorage.clear();
  alert('Local storage limpiado');
}
function tamanioDeLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = localStorage.length;
}